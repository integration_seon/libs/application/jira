import logging
logging.basicConfig(level=logging.INFO)
from .base import Base

class Issue(Base):
	"""
	Class responsable for documentation issues in Jira
	"""
	ERROR = "OS error: {0}"

	def __init__(self, user, apikey, server):
		Base.__init__(self, user, apikey, server)
		
	def find_by_id(self, issue_id): 
		"""
		Responsible for finding all infos from an issue

		Arguments:

			issue_id {Number} -- id of Jira issue

		Returns:
		
			Issue -- Issue object

		"""
		try:
			logging.info("Start function: find_by_id")
			return self.jira.issue(issue_id, expand='changelog')
		except Exception as e: 
			logging.error("OS error: {0}".format(e))
			logging.error(e.__dict__) 

	def find_by_sprint(self, sprint_id):
		"""
		Responsible for finding all issues from a sprint

		Arguments:

			sprint_id {Number} -- id of Jira Sprint

		Returns:
		
			List -- List of all issues from the given sprint

		"""
		try:
			logging.info("Start function: find_by_sprint")
			return self.jira.search_issues(f"Sprint={sprint_id}")
			
		except Exception as e: 
			logging.error("OS error: {0}".format(e))
			logging.error(e.__dict__) 

	def get_by_project_function(self, project, **kwargs):
		result = []
		issues = []

		last_day = kwargs["last_day"]

		try:
			
			function = kwargs["function"]

			logging.info("Start function: get_by_project_function")
			result = self.find_by_project(project['key'], last_day)
			for issue in result:
				value = issue.__dict__
				if 'parent' in value['raw']['fields']:
					value['raw']['fields']['parent'] = self.find_by_id(value['raw']['fields']['parent']['id']).__dict__
				value['project'] = project
				issues.append(value)
				if function is not None:
					function (data=value, topic=kwargs["topic"], extra_data=kwargs["extra_data"])
				
		except Exception as e: 
			logging.error("OS error: {0}".format(e))
			logging.error(e.__dict__) 

		logging.info("Retrieve All Issues")
		return issues

	def find_by_project(self, project_key, last_day=False):
		"""
		Responsible for retreving all issue from a projects
		
		Arguments:

			project_key {String} -- project_key of Jira
		
		Returns:

			List -- Lits of issues of the given project
		
		"""
		try:
			logging.info("Start function: find_by_project")
			if last_day:
				jql_query = (f'project = {project_key} AND updated >= -1d')
				issues = self.jira.search_issues(jql_query, maxResults=100)
				
			else:
				issues = self.jira.search_issues('project='+project_key, maxResults=100)
			tmp = issues
			contador = 100
			while(len(tmp) == 100):
				tmp = self.jira.search_issues(f'project = {project_key}', startAt=contador, maxResults=100)
				issues = issues + tmp
				contador = contador + 100
			return issues
			logging.info("End function: find_by_project")
		except Exception as e: 
			logging.error("OS error: {0}".format(e))
			logging.error(e.__dict__) 

	def find_epic_by_project(self, project_key, time = None):
		"""
		Responsible for finding all project's epics that user has access

		Arguments:

			project_key {String} -- project_key of Jira

		Returns:
		
			List -- List of all issues from the given project with issuetype = 'Epic'

		"""
		return self.__find_all_issue(project_key, 'Epic', time)

	def find_story_by_project(self, project_key, time = None):
		"""
		Responsible for finding all project's storys that user has access

		Arguments:

			project_key {String} -- project_key of Jira

		Returns:
		
			List -- List of all issues from the given project with issuetype = 'Story'

		"""
		return self.__find_all_issue(project_key, 'Story', time)

	def find_task_by_project(self, project_key, time = None):
		"""
		Responsible for finding all project's tasks that user has access

		Arguments:

			project_key {String} -- project_key of Jira

		Returns:
		
			List -- List of all issues from the given project with issuetype = 'Task'

		"""
		return self.__find_all_issue(project_key, 'Task', time)

	def find_subtask_by_project(self, project_key, time = None):
		"""
		Responsible for finding all project's subtasks that user has access

		Arguments:

			project_key {String} -- project_key of Jira

		Returns:
		
			List -- List of all issues from the given project with issuetype = 'Subtask'

		"""
		return self.__find_all_issue(project_key, 'Subtask', time)

	def find_bug_by_project(self, project_key, time = None):
		"""
		Responsible for finding all project's bugs that user has access

		Arguments:

			project_key {String} -- project_key of Jira

		Returns:
		
			List -- List of all issues from the given project with issuetype = 'Bug'

		"""
		return self.__find_all_issue(project_key, 'Bug', time)


	def __find_all_issue(self, project_key, issue_type , time = None):
		jql = f'project = {project_key} AND issuetype = {issue_type}'
		if time is not None:
			jql += f' AND (created>=-{time} OR updated>=-{time})'
		try:
			logging.info("Start function: find_task")
			sub_tasks = self.jira.search_issues(jql,maxResults=100, expand='changelog')
			tmp = sub_tasks
			contador = 100
			while(len(tmp) == 100):
				tmp = self.jira.search_issues(jql,startAt=contador, maxResults=100, expand='changelog')
				sub_tasks = sub_tasks + tmp
				contador = contador + 100
			return sub_tasks
			logging.info("End function: find_task")
		except Exception as e: 
			logging.error("OS error: {0}".format(e))
			logging.error(e.__dict__) 

