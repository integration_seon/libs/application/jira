from pprint import pprint
from jiraX import factories as factory


user = "paulossjunior@gmail.com"
apikey = "ATATT3xFfGF0oiyh-r_2G4wBriV282eOgsD_lTLeggVxqB_OLmlRk__GYc2IKpFc5bTnbfRyerp5RB3P5DhDK1HDq1AZgoHjeQh6clZ97jG0Ur8x-lDUWaSAl_uWWAvkEPTaQELwYuzAsie5fqQTaUkJ7DVSQlQ08b-Kui2vqs3iX7YC3n0T12w=BF8ED9A3"
server =  'https://slaveone-leds.atlassian.net' #Site = organização

project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
user_apl = factory.UserFactory(user=user,apikey=apikey,server=server)
issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
comment_apl = factory.CommentFactory(user=user,apikey=apikey,server=server)
role_apl = factory.RoleFactory(user=user,apikey=apikey,server=server)
backlog_apl = factory.BacklogFactory(user=user,apikey=apikey,server=server)
sprint_backlog_apl = factory.SprintBacklogFactory(user=user,apikey=apikey,server=server)
board_apl = factory.BoardFactory(user=user,apikey=apikey,server=server)
sprint_apl = factory.SprintFactory(user=user,apikey=apikey,server=server)

projects = project_apl.find_all()

for project in projects:
    pprint ("Projeto: "+project.name)

    pprint("Development Team: ")
    users = user_apl.find_by_project(project.key)
    for user in users:
        pprint(user)

    pprint("--------------------")

    pprint("Atomic User Storys: ")
    stories = issue_apl.find_story_by_project(project.key)
    for story in stories:
        pprint(story)

    pprint("--------------------")

    pprint("Epics: ")
    epics = issue_apl.find_epic_by_project(project.key)
    for epic in epics:
        pprint(epic)

    pprint("--------------------")

    pprint("Scrum Tasks: ")
    tasks = issue_apl.find_task_by_project(project.key)
    for task in tasks:
        pprint(task)

    pprint("--------------------")

    pprint("Sprints: ")
    boards = board_apl.find_by_project(project.key)
    sprints = []
    for board in boards:
        sprint_tmp = sprint_apl.find_by_board(board.id)
        if(type(sprint_tmp) != type(None)):
            sprints = sprints+sprint_tmp
    for sprint in sprints:
        pprint(sprint)
    
    pprint("--------------------")

    pprint("Backlog: ")
    backlog = backlog_apl.find_by_project(project.key)
    pprint(backlog)

    pprint("--------------------")

    pprint("Sprint Backlog: ")
    for sprint in sprints:
        sprint_backlog = sprint_backlog_apl.find_by_sprint(sprint.id)
        pprint(sprint_backlog)

    pprint("--------------------")
    pprint(" ")
    pprint(" ")



                