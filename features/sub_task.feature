Feature: Retrive all sub-tasks of a project

    Scenario: get all sub-tasks
    Given a project with sub-tasks
    When i request all sub-tasks of a project to API
    Then i get a list of sub-tasks