from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

user_apl = None
project = None
sprints = None
membros = None
user_id = None
user = None

@given(u'a project2')
def step_impl(context):
    global user_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    user_apl = factory.UserFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 


@when(u'i request all members')
def step_impl(context):
    global sprints
    global membros
    membros = user_apl.find_by_project(project.key)


@then(u'i get a list of members')
def step_impl(context):
    assert membros != []



@given(u'an id3')
def step_impl(context):
    global user_id
    user_id = '557058:0d6aff61-301f-4e03-a3cb-b31d32f923d7'


@when(u'i request the member')
def step_impl(context):
    global user
    user = user_apl.find_by_project_key_and_accountId("STH",user_id)


@then(u'i get a member')
def step_impl(context):
    assert type(user) != type(None)