from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

backlog_apl = None
project = None
backlog = None

@given(u'a project with a non-empty backlog')
def step_impl(context):
    global backlog_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    backlog_apl = factory.BacklogFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH")


@when(u'i request the backlog of a project to API')
def step_impl(context):
    global backlog
    backlog = backlog_apl.find_by_project(project.key)


@then(u'i get the backlog')
def step_impl(context):
    assert backlog != []

    