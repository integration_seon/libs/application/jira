from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

comments = None
issue = None
comments = None


@given(u'a issue2')
def step_impl(context):
    global comment_apl
    global issue
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server = 'https://ledszeppellin.atlassian.net/' #Site = organização
    comment_apl = factory.CommentFactory(user=user,apikey=apikey,server=server)
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    issue = issue_apl.find_by_id(10108)


@when(u'i request all comments of a issue to API')
def step_impl(context):
    global comments
    comments = comment_apl.find_by_issue(issue.id)


@then(u'i get a list of comments')
def step_impl(context):
    assert comments != []
