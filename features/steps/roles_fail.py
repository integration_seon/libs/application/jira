from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

project_apl = None
role_apl = None
project = None
roles = None

@given(u'a false project')
def step_impl(context):
    global project_apl
    global role_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    role_apl = factory.RoleFactory(user=user,apikey=apikey,server=server)
    projects = project_apl.find_all()
    try: 
        project = projects[0]
        project.key = "error"
    except IndexError:
        raise Exception(u'No project found')


@then(u'i fail in request all roles to API')
def step_impl(context):
    global roles
    try:
        roles_in_proj = role_apl.find_by_project(project.key)
        roles = [role_apl.find_by_proj_and_id(project.key,roles_in_proj[role]['id']) for role in roles_in_proj]    
    except Exception as e:
        pass