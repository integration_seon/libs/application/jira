from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

user_apl = None
project = None
sprints = None
membros = None
user_id = None
membro = None

@given(u'a false project2')
def step_impl(context):
    global user_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    user_apl = factory.UserFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 
    project.key = "error"


@then(u'i fail in request all members')
def step_impl(context):
    global sprints
    global membros
    try:
        membros = user_apl.find_by_project(project.key)
    except Exception as e:
        pass


@given(u'an false id3')
def step_impl(context):
    global user_id
    user_id = '00000'


@when(u'i request the false member')
def step_impl(context):
    global membro
    try:
        membro = user_apl.find_by_project_key_and_accountId(user_id, project.key)
    except Exception as e:
        pass

@then(u'i fail to get a member')
def step_impl(context):
    assert membro == None
