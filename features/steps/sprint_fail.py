from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

sprint_apl = None
board_apl = None
project = None
sprints = None

@given(u'a false organization projects')
def given_a_project(context):
    global sprint_apl
    global board_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    sprint_apl = factory.SprintFactory(user=user,apikey=apikey,server=server)
    board_apl = factory.BoardFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 
    project.key = "error"


@then(u'i fail in request all sprints of a project to API')
def step_impl(context):
    global sprints
    try:
        boards = board_apl.find_by_project(project.key)
        sprints = [sprint for board in boards for sprint in sprint_apl.find_by_board(board.id)]
    except Exception as e:
        pass


