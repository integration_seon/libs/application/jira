from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

issues = None
issue_apl = None
sprint = None

project = None


@given(u'a project and a sprint')
def step_impl(context):
    global issue_apl
    global sprint
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    board_apl = factory.BoardFactory(user=user,apikey=apikey,server=server)
    sprint_apl = factory.SprintFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 
    board = board_apl.find_by_project(project.key)[0]
    sprint = sprint_apl.find_by_board(board.id)[0]


@when(u'i request all sprint\'s issues of a project to API')
def step_impl(context):
    global issues
    issues = issue_apl.find_by_sprint(sprint.id)


@then(u'i get a list of issues')
def step_impl(context):
    assert issues != []




@given(u'a project3')
def step_impl(context):
    global issue_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    board_apl = factory.BoardFactory(user=user,apikey=apikey,server=server)
    sprint_apl = factory.SprintFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 


@when(u'i request all issues of a project to API')
def step_impl(context):
    global issues
    issues = issue_apl.find_by_project(project.key)


@then(u'i get a list of project issues')
def step_impl(context):
    assert issues != []



@given(u'a project4')
def step_impl(context):
    global issue_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    board_apl = factory.BoardFactory(user=user,apikey=apikey,server=server)
    sprint_apl = factory.SprintFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 


@when(u'i request all bugs of a project to API')
def step_impl(context):
    global issues
    issues = issue_apl.find_bug_by_project(project.key)


@then(u'i get a list of project bugs')
def step_impl(context):
    assert issues != None