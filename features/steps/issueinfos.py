from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

issue_apl = None
issue = None
infos = None

@given(u'a issue')
def step_impl(context):
    global issue_apl
    global issue
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    issue = issue_apl.find_by_project("STH")[0]


@when(u'i request all infos of the issue to API')
def step_impl(context):
    global infos
    infos = issue_apl.find_by_id(issue.id)


@then(u'i get the issue infos')
def step_impl(context):
    assert infos != None

