from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

issue_apl = None
project = None
epics = None

@given(u'a project with epics')
def given_a_project(context):
    global issue_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_by_key("STH") 


@when(u'i request all epic of a project to API')
def step_impl(context):
    global epics
    epics = issue_apl.find_epic_by_project(project.key)


@then(u'i get a list of epics')
def step_impl(context):
    assert epics != []