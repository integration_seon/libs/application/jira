# Eu, como desenvolvedor, quero recuperar as tasks de um projeto
Feature: Retrive all tasks of a project

    Scenario: get all tasks
    Given a project with tasks
    When i request all tasks of a project to API
    Then i get a list of tasks