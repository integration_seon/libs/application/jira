# Eu, como desenvolvedor, quero recuperar 
# as dados das tarefas de um projeto dentro de um sprint.

Feature: Retrive all issues 

    Scenario: get all issues of a sprint
    Given a project and a sprint
    When i request all sprint's issues of a project to API
    Then i get a list of issues

    Scenario: get all issues of a project
    Given a project3
    When i request all issues of a project to API
    Then i get a list of project issues

    Scenario: get all bugs of a project
    Given a project4
    When i request all bugs of a project to API
    Then i get a list of project bugs
